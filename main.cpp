#include "inclusions.h"
#include "sentence_task.h"
#include "collection.h"
#include "replacement.h"


int main(int argc, char *argv[])
{


    Collection collection(SOURCE_SENTENCE_INPUT_FILE);
    collection.read_full_file();


    int i=0;
    while ( true)
     {
        if ( collection.no_more_lines() )
            break;

       Sentence sentence(collection.get_line() );  //

       if ( sentence.parse_sentence() ==-1) // comment string #
            continue;

        Replacement replacement(sentence);
        replacement.generate_words_from_wn(); // to show results to user


        if ( replacement.show_invitation_edit() == 0 ) // sentence has been chosen to be skipped by user
         {
            cerr<<"processing of sentence has been cancelled by you\n";
            continue;

         }
//       replacement.generate_words_from_wn();// for every word???



        replacement.generate_permutations();
        cout<<replacement;

        cerr<<"sentence has been processed successfully\n";
      cerr<<endl;

//        return 0;
//        if ( ++i==2)
//            break;
//        sentence.show_separately();

      }


    fprintf(stderr,"application has finished successfully, you  can look at results in out.txt file\n");
    return 0;
}

