#include "replacement.h"
#include "sentence_wn.h"
//char Libmagic_check::marchive_formats[NUMBER_ARCHIVE_IDENTIFIERS][_MAX_LENGTH_ONE_PARAMETER]={"zip","x-gzip","x-7z-compressed","x-rar","x-xz","x-tar","x-bzip2"};

const char Replacement::kREPLACEMENT_TEMP_FILE_[] =  "temp.txt";
const char Replacement::kREPLACEMENT_VERB_[] = "v";
const char Replacement::kREPLACEMENT_NOUN_[] = "n";
const char Replacement::kREPLACEMENT_ADJECTIVE_[] = "a";
const char Replacement::kREPLACEMENT_ADVERB_[] = "r";
vector<string> Replacement::mgenerated_strings;

Replacement::Replacement(Sentence &sentence) {
  msentence = &sentence;
  this->check_correctness_of_requested_transformation();
}


void Replacement::check_correctness_of_requested_transformation() {

//    string a(REPLACEMENT_ADVERB);
    for(size_t i=0; i<msentence->number_of_words_in_sentence(); i++)
        if ( msentence->mstorage_words[i].msearch == true &&
             !equal( msentence->mstorage_words[i].part_of_speech.begin(),
                     msentence->mstorage_words[i].part_of_speech.end(),string(kREPLACEMENT_ADJECTIVE_).begin())
             && !equal( msentence->mstorage_words[i].part_of_speech.begin(),
                                     msentence->mstorage_words[i].part_of_speech.end(),string(kREPLACEMENT_ADVERB_).begin())
             && !equal( msentence->mstorage_words[i].part_of_speech.begin(),
                                    msentence->mstorage_words[i].part_of_speech.end(),string(kREPLACEMENT_VERB_).begin())
             && !equal( msentence->mstorage_words[i].part_of_speech.begin(),
                                     msentence->mstorage_words[i].part_of_speech.end(),string(kREPLACEMENT_NOUN_).begin())
             )
        {
            cout<<msentence->mstorage_words[i];
            ERROR_EXIT("wrong request - for this part of speech, it  is not supported, tip -  delete @ sign near it")
        }
}


string Replacement::form_command(size_t i) {
    string command = "wn ";

//    command+="good";
    command += msentence->mstorage_words[i].word;
    command += " ";

    command += "-";
    command += msentence->mstorage_words[i].generation_part_of_speech;
    command += msentence->mstorage_words[i].part_of_speech;
    command += " > ";
    command += kREPLACEMENT_TEMP_FILE_;


    cout<<command<<endl;
   return command;

}


int Replacement::show_invitation_edit() {

  static int counter_of_sentences = 1;
  if (counter_of_sentences == 1)  {
      fprintf(stderr,INFO);
   }
  fprintf(stderr,"%d %s",counter_of_sentences++, " sentence is being processed\n");
//   char a=getchar();
//   while( getchar());
//   a='y';

  char a = 'y';
//    scanf("%d",&a);

  a = cin.get();
  cin.ignore(1);
  cin.sync();

//  if (a == 's')
//  {
//      return -1;
//  }
     if (( a!='y') && (a!='Y' ))  {

         fprintf(stderr,"application exited because you cancelled to continue\n");

//        fflush(stderr);
//        fflush(stdin);
       return 0; // no, sentence will be skipped
    }    else  {
        fprintf(stderr,"work is going on\n");

//        fflush(stderr);
//        fflush(stdin);


         return 1;
    }


}



void Replacement::generate_words_from_wn() {

 for( int i=0; i<msentence->mnumber_of_words; i++)
  if (msentence->mstorage_words[i].msearch == true)  {
//   string  command=form_command(i);
//    fclose(stdout);
    system(form_command(i).c_str());

    Collection   wordnet(kREPLACEMENT_TEMP_FILE_);
    wordnet.read_full_file();
//    wordnet.show();

//   if (wordnet.is_empty())
//       return;

    while ( !wordnet.no_more_lines())  {

        Sentence_wn sentence(wordnet.get_line() );
        sentence.parse_sentence();

        set<string> &loc_set = sentence.get_mwords();
        msentence->mstorage_words[i].mholder_replace.insert( loc_set.begin(),loc_set.end() );
     }
  }

 fflush(stdout);
}

void Replacement::generate_permutations() {

    size_t number_of_words = msentence->mnumber_of_words;

    size_t combinations[number_of_words];
    int positions[number_of_words];

    memset(combinations,0,number_of_words*sizeof(int));
    memset(positions,0,number_of_words*sizeof(int));

    cout<<"positions\n";
    for(size_t i=0; i<number_of_words; i++)
    {  positions[i]=msentence->mstorage_words[i].msearch;
       cout<<positions[i]<<" ";

    }

    size_t quantity_of_words[number_of_words], copy_quantity_of_words[number_of_words];

    cout<<"\nquantity";
   for(int i=0;i<number_of_words;i++)
      copy_quantity_of_words[i]=quantity_of_words[i]=msentence->mstorage_words[i].mholder_replace.size();

   cout<<endl;

   for(int i=0; i<number_of_words; i++) cout<<quantity_of_words[i]<<" ";

    cout<<endl;

    set<string>::iterator iter;

    int i = 0;
    while(++i<number_of_words)  {

        if ( quantity_of_words[i]!=0)   {
            string was_string = msentence->mstorage_words[i].word;
            iter = msentence->mstorage_words[i].mholder_replace.begin();

//          std::advance(iter,msentence->mstorage_words[i].mnumber_word_from_mholder_replace);

            for(int i = 0;i<msentence->mstorage_words[i].mnumber_word_from_mholder_replace
                &&  iter != msentence->mstorage_words[i].mholder_replace.end(); i++)
                iter++;

            //first internal circle of exhaustion
            for(int j=0;j<quantity_of_words[i] && iter!=msentence->mstorage_words[i].mholder_replace.end();j++)  {
//                msentence->show_current();
                mgenerated_strings.push_back( msentence->form_sentence());
                mgenerated_strings.push_back( "\n");
                msentence->mstorage_words[i]=*iter++;
            }
//               msentence->show_current();
               mgenerated_strings.push_back(msentence->form_sentence());
               mgenerated_strings.push_back( "\n");
               msentence->mstorage_words[i].word=was_string; //restore previous

//               Word word=msentence->mstorage_words[i];


               for ( int k=i+1;k<number_of_words; k++)
                  if (quantity_of_words[k]!=0)   {
                      quantity_of_words[k]--;
                      set<string>::iterator ittt;
                     ittt=msentence->mstorage_words[k].mholder_replace.begin();

                     for(int i=0;i<msentence->mstorage_words[k].mnumber_word_from_mholder_replace &&
                         ittt!=msentence->mstorage_words[k].mholder_replace.end();i++)
                         ittt++;

                     msentence->mstorage_words[k].mnumber_word_from_mholder_replace++; // not to take into account first word
                     msentence->mstorage_words[k].word=*ittt;


//                     Word word=msentence->mstorage_words[k];
                        for(int i=0;i<k;i++)  {
                            msentence->mstorage_words[i].mnumber_word_from_mholder_replace=0;
                            quantity_of_words[i]=copy_quantity_of_words[i];
                         }

                        i=0;
                   break;

               }

        }
    }


}
