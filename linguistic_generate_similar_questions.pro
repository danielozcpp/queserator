TEMPLATE = app
CONFIG += console
CONFIG += qt

SOURCES += main.cpp \
    word.cpp \
    collection.cpp \
    replacement.cpp \
    sentence_task.cpp \
    sentence_wn.cpp

HEADERS += \
    inclusions.h \
    word.h \
    collection.h \
    replacement.h \
    sentence_task.h \
    sentence_wn.h

OTHER_FILES +=

