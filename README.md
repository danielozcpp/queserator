Simple project for generation of questions from  seed sentences


Desciption
This is not a simple game with shifting order of words, because
sentences still hold sense (some) after processing.

Of course, we can state than only human can generate perfect questions, 
but it's  just a step into this direction.

Every human goes through a long way of learning until he or she becomes able
to think up plausible questions (since childhood).
Moreover even not every grown-up man can always ask only "correct" questions.
Under the word "correct" is meant those questions which have a sence and are completely grammatically correct.


INPUT settings are in input.txt

# EXAMPLE 
E.g. you give to input two sentences like:
 what is bigger fly or tiger?
 
 what is the colour of your hair?

As result program generates automatically questions with the same sense. 
It can be applicable not only  to questions but to any sentences. 

#First sentence

what is bigger fly or tiger?
what is bigger  dipteran or tiger?
what is bigger  dipteron or tiger?
what is bigger  fish lure or tiger?
what is bigger  hitting or tiger?
what is bigger  striking or tiger?
what is bigger  two-winged insects or tiger?
what is bigger dipterous insect or tiger?
what is bigger fisherman's lure or tiger?
what is bigger flap or tiger?
what is bigger hit or tiger?
what is bigger opening or tiger?
what is bigger fly or  cat?
what is bigger  dipteran or  cat?
what is bigger  dipteron or  cat?
what is bigger  fish lure or  cat?
what is bigger  hitting or  cat?
what is bigger  striking or  cat?
what is bigger  two-winged insects or  cat?
what is bigger dipterous insect or  cat?
what is bigger fisherman's lure or  cat?
what is bigger flap or  cat?
what is bigger hit or  cat?
what is bigger opening or  cat?
what is bigger fly or  individual?
what is bigger  dipteran or  individual?
what is bigger  dipteron or  individual?
what is bigger  fish lure or  individual?
what is bigger  hitting or  individual?
what is bigger  striking or  individual?
what is bigger  two-winged insects or  individual?
what is bigger dipterous insect or  individual?
what is bigger fisherman's lure or  individual?
what is bigger flap or  individual?
what is bigger hit or  individual?
what is bigger opening or  individual?
what is bigger fly or  mortal?
what is bigger  dipteran or  mortal?
what is bigger  dipteron or  mortal?
what is bigger  fish lure or  mortal?
what is bigger  hitting or  mortal?
what is bigger  striking or  mortal?
what is bigger  two-winged insects or  mortal?
what is bigger dipterous insect or  mortal?
what is bigger fisherman's lure or  mortal?
what is bigger flap or  mortal?
what is bigger hit or  mortal?
what is bigger opening or  mortal?
what is bigger fly or  somebody?
what is bigger  dipteran or  somebody?
what is bigger  dipteron or  somebody?
what is bigger  fish lure or  somebody?
what is bigger  hitting or  somebody?
what is bigger  striking or  somebody?
what is bigger  two-winged insects or  somebody?
what is bigger dipterous insect or  somebody?
what is bigger fisherman's lure or  somebody?
what is bigger flap or  somebody?
what is bigger hit or  somebody?
what is bigger opening or  somebody?
what is bigger fly or  someone?
what is bigger  dipteran or  someone?
what is bigger  dipteron or  someone?
what is bigger  fish lure or  someone?
what is bigger  hitting or  someone?
what is bigger  striking or  someone?
what is bigger  two-winged insects or  someone?
what is bigger dipterous insect or  someone?
what is bigger fisherman's lure or  someone?
what is bigger flap or  someone?
what is bigger hit or  someone?
what is bigger opening or  someone?
what is bigger fly or  soul?
what is bigger  dipteran or  soul?
what is bigger  dipteron or  soul?
what is bigger  fish lure or  soul?
what is bigger  hitting or  soul?
what is bigger  striking or  soul?
what is bigger  two-winged insects or  soul?
what is bigger dipterous insect or  soul?
what is bigger fisherman's lure or  soul?
what is bigger flap or  soul?
what is bigger hit or  soul?
what is bigger opening or  soul?
what is bigger fly or big cat?
what is bigger  dipteran or big cat?
what is bigger  dipteron or big cat?
what is bigger  fish lure or big cat?
what is bigger  hitting or big cat?
what is bigger  striking or big cat?
what is bigger  two-winged insects or big cat?
what is bigger dipterous insect or big cat?
what is bigger fisherman's lure or big cat?
what is bigger flap or big cat?
what is bigger hit or big cat?
what is bigger opening or big cat?
what is bigger fly or person?
what is bigger  dipteran or person?
what is bigger  dipteron or person?
what is bigger  fish lure or person?
what is bigger  hitting or person?
what is bigger  striking or person?
what is bigger  two-winged insects or person?
what is bigger dipterous insect or person?
what is bigger fisherman's lure or person?
what is bigger flap or person?
what is bigger hit or person?
what is bigger opening or person?


#Second sentence

what is the colour of your hair?
what is the  form of your hair?
what is the  interestingness of your hair?
what is the  quality of your hair?
what is the  sort of your hair?
what is the  stuff of your hair?
what is the  timber of your hair?
what is the  tone of your hair?
what is the  variety of your hair?
what is the  visual aspect of your hair?
what is the appearance of your hair?
what is the interest of your hair?
what is the kind of your hair?
what is the material of your hair?
what is the race of your hair?
what is the timbre of your hair?
what is the visual property of your hair?
what is the colour of your  appendage?
what is the  form of your  appendage?
what is the  interestingness of your  appendage?
what is the  quality of your  appendage?
what is the  sort of your  appendage?
what is the  stuff of your  appendage?
what is the  timber of your  appendage?
what is the  tone of your  appendage?
what is the  variety of your  appendage?
what is the  visual aspect of your  appendage?
what is the appearance of your  appendage?
what is the interest of your  appendage?
what is the kind of your  appendage?
what is the material of your  appendage?
what is the race of your  appendage?
what is the timbre of your  appendage?
what is the visual property of your  appendage?
what is the colour of your  cloth?
what is the  form of your  cloth?
what is the  interestingness of your  cloth?
what is the  quality of your  cloth?
what is the  sort of your  cloth?
what is the  stuff of your  cloth?
what is the  timber of your  cloth?
what is the  tone of your  cloth?
what is the  variety of your  cloth?
what is the  visual aspect of your  cloth?
what is the appearance of your  cloth?
what is the interest of your  cloth?
what is the kind of your  cloth?
what is the material of your  cloth?
what is the race of your  cloth?
what is the timbre of your  cloth?
what is the visual property of your  cloth?
what is the colour of your  enation?
what is the  form of your  enation?
what is the  interestingness of your  enation?
what is the  quality of your  enation?
what is the  sort of your  enation?
what is the  stuff of your  enation?
what is the  timber of your  enation?
what is the  tone of your  enation?
what is the  variety of your  enation?
what is the  visual aspect of your  enation?
what is the appearance of your  enation?
what is the interest of your  enation?
what is the kind of your  enation?
what is the material of your  enation?
what is the race of your  enation?
what is the timbre of your  enation?
what is the visual property of your  enation?
what is the colour of your  filum?
what is the  form of your  filum?
what is the  interestingness of your  filum?
what is the  quality of your  filum?
what is the  sort of your  filum?
what is the  stuff of your  filum?
what is the  timber of your  filum?
what is the  tone of your  filum?
what is the  variety of your  filum?
what is the  visual aspect of your  filum?
what is the appearance of your  filum?
what is the interest of your  filum?
what is the kind of your  filum?
what is the material of your  filum?
what is the race of your  filum?
what is the timbre of your  filum?
what is the visual property of your  filum?
what is the colour of your  material?
what is the  form of your  material?
what is the  interestingness of your  material?
what is the  quality of your  material?
what is the  sort of your  material?
what is the  stuff of your  material?
what is the  timber of your  material?
what is the  tone of your  material?
what is the  variety of your  material?
what is the  visual aspect of your  material?
what is the appearance of your  material?
what is the interest of your  material?
what is the kind of your  material?
what is the material of your  material?
what is the race of your  material?
what is the timbre of your  material?
what is the visual property of your  material?
what is the colour of your  outgrowth?
what is the  form of your  outgrowth?
what is the  interestingness of your  outgrowth?
what is the  quality of your  outgrowth?
what is the  sort of your  outgrowth?
what is the  stuff of your  outgrowth?
what is the  timber of your  outgrowth?
what is the  tone of your  outgrowth?
what is the  variety of your  outgrowth?
what is the  visual aspect of your  outgrowth?
what is the appearance of your  outgrowth?
what is the interest of your  outgrowth?
what is the kind of your  outgrowth?
what is the material of your  outgrowth?
what is the race of your  outgrowth?
what is the timbre of your  outgrowth?
what is the visual property of your  outgrowth?
what is the colour of your  small indefinite amount?
what is the  form of your  small indefinite amount?
what is the  interestingness of your  small indefinite amount?
what is the  quality of your  small indefinite amount?
what is the  sort of your  small indefinite amount?
what is the  stuff of your  small indefinite amount?
what is the  timber of your  small indefinite amount?
what is the  tone of your  small indefinite amount?
what is the  variety of your  small indefinite amount?
what is the  visual aspect of your  small indefinite amount?
what is the appearance of your  small indefinite amount?
what is the interest of your  small indefinite amount?
what is the kind of your  small indefinite amount?
what is the material of your  small indefinite amount?
what is the race of your  small indefinite amount?
what is the timbre of your  small indefinite amount?
what is the visual property of your  small indefinite amount?
what is the colour of your  textile?
what is the  form of your  textile?
what is the  interestingness of your  textile?
what is the  quality of your  textile?
what is the  sort of your  textile?
what is the  stuff of your  textile?
what is the  timber of your  textile?
what is the  tone of your  textile?
what is the  variety of your  textile?
what is the  visual aspect of your  textile?
what is the appearance of your  textile?
what is the interest of your  textile?
what is the kind of your  textile?
what is the material of your  textile?
what is the race of your  textile?
what is the timbre of your  textile?
what is the visual property of your  textile?
what is the colour of your body covering?
what is the  form of your body covering?
what is the  interestingness of your body covering?
what is the  quality of your body covering?
what is the  sort of your body covering?
what is the  stuff of your body covering?
what is the  timber of your body covering?
what is the  tone of your body covering?
what is the  variety of your body covering?
what is the  visual aspect of your body covering?
what is the appearance of your body covering?
what is the interest of your body covering?
what is the kind of your body covering?
what is the material of your body covering?
what is the race of your body covering?
what is the timbre of your body covering?
what is the visual property of your body covering?
what is the colour of your fabric?
what is the  form of your fabric?
what is the  interestingness of your fabric?
what is the  quality of your fabric?
what is the  sort of your fabric?
what is the  stuff of your fabric?
what is the  timber of your fabric?
what is the  tone of your fabric?
what is the  variety of your fabric?
what is the  visual aspect of your fabric?
what is the appearance of your fabric?
what is the interest of your fabric?
what is the kind of your fabric?
what is the material of your fabric?
what is the race of your fabric?
what is the timbre of your fabric?
what is the visual property of your fabric?
what is the colour of your filament?
what is the  form of your filament?
what is the  interestingness of your filament?
what is the  quality of your filament?
what is the  sort of your filament?
what is the  stuff of your filament?
what is the  timber of your filament?
what is the  tone of your filament?
what is the  variety of your filament?
what is the  visual aspect of your filament?
what is the appearance of your filament?
what is the interest of your filament?
what is the kind of your filament?
what is the material of your filament?
what is the race of your filament?
what is the timbre of your filament?
what is the visual property of your filament?
what is the colour of your plant process?
what is the  form of your plant process?
what is the  interestingness of your plant process?
what is the  quality of your plant process?
what is the  sort of your plant process?
what is the  stuff of your plant process?
what is the  timber of your plant process?
what is the  tone of your plant process?
what is the  variety of your plant process?
what is the  visual aspect of your plant process?
what is the appearance of your plant process?
what is the interest of your plant process?
what is the kind of your plant process?
what is the material of your plant process?
what is the race of your plant process?
what is the timbre of your plant process?
what is the visual property of your plant process?
what is the colour of your process?
what is the  form of your process?
what is the  interestingness of your process?
what is the  quality of your process?
what is the  sort of your process?
what is the  stuff of your process?
what is the  timber of your process?
what is the  tone of your process?
what is the  variety of your process?
what is the  visual aspect of your process?
what is the appearance of your process?
what is the interest of your process?
what is the kind of your process?
what is the material of your process?
what is the race of your process?
what is the timbre of your process?
what is the visual property of your process?
what is the colour of your small indefinite quantity?
what is the  form of your small indefinite quantity?
what is the  interestingness of your small indefinite quantity?
what is the  quality of your small indefinite quantity?
what is the  sort of your small indefinite quantity?
what is the  stuff of your small indefinite quantity?
what is the  timber of your small indefinite quantity?
what is the  tone of your small indefinite quantity?
what is the  variety of your small indefinite quantity?
what is the  visual aspect of your small indefinite quantity?
what is the appearance of your small indefinite quantity?
what is the interest of your small indefinite quantity?
what is the kind of your small indefinite quantity?
what is the material of your small indefinite quantity?
what is the race of your small indefinite quantity?
what is the timbre of your small indefinite quantity?
what is the visual property of your small indefinite quantity?

