#include "sentence_task.h"

int Sentence::number_of_processed_lines_=0;

Sentence::Sentence() {

}

Sentence::Sentence(string line){
  mline_input=line;
  mend_of_sentences=false;
  mstorage_words=NULL;
  mnumber_of_words=0;
  set_question_mark=0;
  ++number_of_processed_lines_;

}

Sentence::~Sentence() {
   mline_input.clear();

    if (mstorage_words != NULL)  {
        delete []mstorage_words;
        mstorage_words = NULL;
    }
}

Word Sentence::parse_word(string &word) {
  string::iterator iter1=word.begin();

  iter1=find(word.begin(),word.end(),'/');

  if(iter1 == word.end())  {
      cout<<number_of_processed_lines_<<" line is malformed, counting is started from 1 not zero \n";
      ERROR_EXIT("error of input in this line");
    }
  else {
  Word word_to_return;
  word_to_return.word.assign(word.begin(),iter1);

  string::iterator iter2=find(word.begin(),word.end(),SENTENCE_RESERVED_DIVISOR);
  if ( iter2!= word.end())  {
      word_to_return.part_of_speech.assign(++iter1,iter2);
      word_to_return.msearch=true;

      if ( *--word.end()=='?' ) {
         word_to_return.generation_part_of_speech.assign(++iter2,--word.end());
         this->set_question_mark=1;
      }  else {
        word_to_return.generation_part_of_speech.assign(++iter2,word.end());
      }
     }  else {
    word_to_return.part_of_speech.assign(++iter1,word.end());
   }

  return word_to_return;
 }
}


int Sentence::parse_sentence() {
 mstorage_words = NULL;

 if ( mline_input[0] == '#')  return -1; // ignore comments

  size_t number_words=this->number_of_words_in_sentence();
  mnumber_of_words=number_words;

  mstorage_words=new Word[number_words];

  string::iterator iter1=mline_input.begin();
  string::iterator iter2=mline_input.begin();
  size_t i=0;

  while( iter1 != mline_input.end())   {
     iter1=find(iter2,mline_input.end(),' ');
     string line(iter2,iter1);

       Word another=parse_word(line);
       mstorage_words[i++]=another;

     if(iter1 == mline_input.end())  break;

     iter1++;
     iter2=iter1;
   }

}


size_t Sentence::number_of_words_in_sentence() {
//   string &current_line=mline_input;

    string::iterator iter1=mline_input.begin();
    string::iterator iter2=mline_input.begin();
  size_t counter=0;

  while( iter1 != mline_input.end())   {
     counter++;
     iter1 = find(iter2,mline_input.end(),' ');

     if(iter1 == mline_input.end())  break;

     iter1++;
     iter2 = iter1;
    }

  return counter;
}

void Sentence::show_together() {
        for(size_t i=0;i<mnumber_of_words; i++)
          cout<<mstorage_words[i];

}


void Sentence::show_separately() {
    for(size_t i=0;i<mnumber_of_words;i++)
        cout<<mstorage_words[i].word<<"  "<<mstorage_words[i].part_of_speech<<"  "<<mstorage_words[i].msearch<<"\n";
}


void Sentence::show_generated() {

    cout<<"\n\n";

    for(size_t i=0; i< mnumber_of_words; i++) {
        cout<<mstorage_words[i].word<<"\n";
        if (mstorage_words[i].msearch == true) {
            set<string>::iterator iter;

            for(iter=mstorage_words[i].mholder_replace.begin(); iter!=mstorage_words[i].mholder_replace.end(); iter++)
                cout<<*iter<<" ";

        }

    }
}

void Sentence::show_current() {
    cout<<"\n";
    for(size_t i=0; i< mnumber_of_words; i++)
      cout<<mstorage_words[i].word<<" ";
}

string Sentence::form_sentence() {
    string line;

    for(size_t i=0;i<mnumber_of_words;i++) {
        line.append(mstorage_words[i].word);
        line.push_back(' ');
    }

    if (this->set_question_mark)  line[line.length()-1]='?';

    return line;
}
