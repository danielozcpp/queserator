#ifndef WORD_H
#define WORD_H

#include "inclusions.h"

class Word {
 public:
  string part_of_speech,word,generation_part_of_speech;
  bool msearch;
  set<string> mholder_replace;
  size_t mnumber_word_from_mholder_replace;

  Word();
  Word operator=( Word &source)  {
          part_of_speech.clear();
          word.clear();
          generation_part_of_speech.clear();

           this->word.assign(source.word.begin(),source.word.end());
           this->part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
           this->msearch=source.msearch;
           this->generation_part_of_speech=source.generation_part_of_speech;
          return *this;
  }


  Word operator=( string &source)  {
//          part_of_speech.clear();
//          word.clear();
//          generation_part_of_speech.clear();

           this->word.assign(source.begin(),source.end());
//           this->part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
//           this->msearch=source.msearch;
//           this->generation_part_of_speech=source.generation_part_of_speech;
          return *this;
  }



  Word operator[](string source)  {

  }

  Word operator=( string source)  {
//          part_of_speech.clear();
//          word.clear();
//          generation_part_of_speech.clear();

           this->word.assign(source.begin(),source.end());
//           this->part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
//           this->msearch=source.msearch;
//           this->generation_part_of_speech=source.generation_part_of_speech;
          return *this;
  }



//  Word operator&=( Word &source)
//  { /* Word for_return_word;
//      for_return_word.word.assign(source.word.begin(),source.word.end());
//      for_return_word.part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
//     return for_return_word;*/
//          part_of_speech.clear();
//          word.clear();

//           this->word.assign(source.word.begin(),source.word.end());
//           this->part_of_speech.assign(source.part_of_speech.begin(),source.part_of_speech.end());
//          return *this;
//  }

  friend ostream &operator<<(ostream &stream, Word &source)  {
      stream<<source.word<<"  "<<source.part_of_speech<<"  "<<source.msearch<<"  "<< source.generation_part_of_speech<<"\n";
  }

};


#endif // WORD_H
