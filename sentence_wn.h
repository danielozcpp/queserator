#ifndef SENTENCE_WN_H
#define SENTENCE_WN_H

#include "sentence_task.h"
#include "inclusions.h"


class Sentence_wn : public Sentence {
    #define SENTENCE_WN_PATTERN_FOR_SEARCH "=>"
public:
    set<string> mwords;
    Sentence_wn(string line);
    void parse_sentence();
    void push_words(string &line,string::iterator &iter);
    void show_together();

    set<string> &get_mwords() {  return mwords; }

//    Word parse_word(string &word);
};


#endif // SENTENCE_WN_H
