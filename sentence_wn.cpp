#include "sentence_wn.h"

Sentence_wn::Sentence_wn(string line) {
    mline_input=line;
    mend_of_sentences=false;
    mstorage_words=NULL;
}


void Sentence_wn::show_together() {
    set<string>::iterator iter;

    for(iter=mwords.begin(); iter!=mwords.end();iter++)
        cout<<*iter<<" | ";
}

void Sentence_wn::push_words(string &line, string::iterator &iter) {
   iter+=3; // move position to the beginning of the word
   string::iterator liter=iter, prev=iter;


  while ( true)  {
   prev=liter;
   liter=find(liter,line.end(),',');

   string loc_line(prev,liter);
   mwords.insert(loc_line); // only unique values will be inserted

   if (liter== line.end())  break;

   liter++;
  }

}

void Sentence_wn::parse_sentence() {
  string::iterator iter1;

  char pattern[]=SENTENCE_WN_PATTERN_FOR_SEARCH;

  iter1=search(mline_input.begin(),mline_input.end(),pattern,pattern+strlen(pattern));

  if( iter1 == mline_input.end())    return;  // not found

  push_words(mline_input,iter1);

}

