#ifndef INCLUSIONS_H
#define INCLUSIONS_H

using namespace std;
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cstring>
#include <algorithm>
#include <set>
#include <map>


#include <QApplication>
#include <QMessageBox>

#define  SOURCE_WORD_NET "output.txt"
#define SOURCE_SENTENCE_INPUT_FILE "input.txt"


#define ERROR_EXIT(line)\
{  \
    cout<<__FILE__<<"  " << __LINE__ << "  "<<line<<endl; \
    exit(0);   \
}


#define INFO "words are generated, you can make changes in temp.txt in the current folder, after editing enter 'yes' to go on or any key to \
exit\n  or  enter 'skip' to skip manual edition at all \n\n"

#endif // INCLUSIONS_H
