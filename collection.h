#ifndef SOURCE_SENTENCE_H
#define SOURCE_SENTENCE_H

#include  "inclusions.h"
#include "word.h"

/***** GENERAL REPRESENTATION OF  SENTENCES READ FROM FILE  *****/

class Collection {
 public:
    explicit Collection(const char *input_file);

    void read_full_file();
    void reset_to_first_line() { mcurrent_line=mcontainer.begin(); }
    string get_line();
    char no_more_lines() { return mno_more_lines; }
    size_t get_number_of_lines() { return mcontainer.size(); }
    void show();
    ~Collection();

 private:
    //  #define DEBUG_SOURCE_SENTENCES
      const static size_t mstandard_length_of_sentence=10;

      char *mstorage;
      size_t increase_size;
      ifstream minput_data;
      vector<string> mcontainer;
      vector<string>::iterator mcurrent_line;
      char mno_more_lines;
      size_t mcount_of_given_lines;


      int read_line();
};

#endif // SOURCE_SENTENCE_H
